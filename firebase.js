import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyA0G8EI2KmJuEDV5Wh5dbWFJwMRuNzBhR8",
  authDomain: "clone-5c5d1.firebaseapp.com",
  projectId: "clone-5c5d1",
  storageBucket: "clone-5c5d1.appspot.com",
  messagingSenderId: "337461028912",
  appId: "1:337461028912:web:b4cd32f81824b3983665f5",
};

const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();

export { app, db };
