const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

export default async (req, res) => {
  const { items, email } = req.body;

  const line_items = items.map((item) => ({
    price_data: {
      currency: "GBP",
      product_data: {
        name: item.title,
        description: item.description,
        images: [item.image],
      },
      unit_amount: item.price * 100,
    },
    quantity: 1,
  }));

  const session = await stripe.checkout.sessions.create({
    customer_email: email,
    shipping_rates: ["shr_1KFpGwCqSlqQJkas9NLb3I8S"],
    shipping_address_collection: {
      allowed_countries: ["GB", "US", "CA", "FR"],
    },
    line_items,
    mode: "payment",
    success_url: `${process.env.VERCEL_URL}/success`,
    cancel_url: `${process.env.VERCEL_URL}/checkout`,
    metadata: {
      email,
      images: JSON.stringify(items.map((item) => item.image)),
    },
  });

  res.status(200).json({ id: session.id });
};
